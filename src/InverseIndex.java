import java.util.*;

public class InverseIndex {
	
    private Hashtable<String, int[]> documents;
    private Hashtable<String, TermWithFeatures> terms;
    
    private int documentIndex_counter, termIndex_counter;
    private int[] documentFeatures;

    TermWithFeatures termWithFeatures;
    DocWithPositions docWithPositions;
    
    InverseIndex() {
        documents = new Hashtable<String, int[]>();
        documentIndex_counter = 0;
        
        terms = new Hashtable<String, TermWithFeatures>();
        termIndex_counter = 0;
    }
    
    public void addTerms(String document, String[] newTerms) {
        if (!documents.contains(document)) {
                
            addDocument(document);
            int docId = documents.get(document)[0]; //this is stupid
            
            int newTerm_index = 0;
            for (String term: newTerms) {
                if (terms.contains(term)) {
                    termWithFeatures = terms.get(term);
                    if (termWithFeatures.isTermContains(docId)) {
                        docWithPositions = termWithFeatures.getDocWithPositions(docId);
                        docWithPositions.addTermPosition(newTerm_index);
                    } else {
                        termWithFeatures.addToPostings(docId);
                        docWithPositions = termWithFeatures.getDocWithPositions(docId);
                        docWithPositions.addTermPosition(newTerm_index);
                    }
                } else {
                    addTerm(term);
                    
                    termWithFeatures = terms.get(term);
                    termWithFeatures.addToPostings(docId);
                    docWithPositions = termWithFeatures.getDocWithPositions(docId);
                    docWithPositions.addTermPosition(newTerm_index);
                }
                newTerm_index++;
            }
        }
    }
    
    private void addDocument(String document) {
        if (!documents.contains(document)) {
            documentFeatures = new int[2];
            documentFeatures[0] = documentIndex_counter;
            documentFeatures[1] = -1;
            documents.put(document, documentFeatures);
            documentIndex_counter++;
        }
    }
    
    private void addTerm(String term) {
        terms.put(term, new TermWithFeatures(termIndex_counter));
        termIndex_counter++;
    }

    public void computeDocLenghts() {
            
    }
    
    public boolean isItNewDocument(String document) {
        if (documents.contains(document)) return false;
        return true;
    }
    
    public boolean isItNewTerm(String term) {
        if (terms.contains(term)) return false;
        return true;
    }
    
    public int getTermFrequency(String term, String document) {
        return 0;
    }
    
    public int getDocFrequency(String term) {
        return 0;
    }
    
    public Set<String> getDocNames() {
        return documents.keySet();
    }
    
    class TermWithFeatures {
            
        private int termIndex;
        private int numberOfDocuments;
        private Hashtable<Integer, DocWithPositions> termDocuments;
        
        TermWithFeatures(int termIndex) {
            this.termIndex = termIndex;
            numberOfDocuments = 0;
            termDocuments = new Hashtable<Integer, DocWithPositions>();
        }
        
        public int getTermIndex() {
            return termIndex;
        }
        
        public int getSizeOfPostingsList() {
            return numberOfDocuments;
        }
        
        public void addToPostings(int docId) {
            termDocuments.put(docId, new DocWithPositions(docId, termIndex));
            numberOfDocuments++;
        }
        
        public boolean isTermContains(int docId) {
            if (termDocuments.contains(docId)) return true;
            return false;
        }
        
        public DocWithPositions getDocWithPositions(int docId) {
            return termDocuments.get(docId);
        }
        
    }
    
    class DocWithPositions {
            
        private int docIndex;
        private int termIndex;
        private int termFrequency;
        private List<Integer> termPositions; // can be hash set
        
        DocWithPositions(int docIndex, int termIndex) {
            this.docIndex = docIndex;
            this.termIndex = termIndex;
            termFrequency = 0;
            termPositions = new LinkedList<Integer>();
        }
        
        public int getDocIndex() {
            return docIndex;
        }
        
        public int getTermIndex() {
            return termIndex;
        }
        
        public int getTermFrequency() {
            return termFrequency;
        }

        public void addTermPosition(int position) {
            termPositions.add(position);
            termFrequency++;
        }
        
    }
}
