import java.util.*;

public class QueryProcessor
{
    private PositionalIndex positionalIndex;
    private Set<String> documents;
    
    QueryProcessor(String folderName) {
        //TODO: does it need a constructor?
        
        positionalIndex = new PositionalIndex(folderName);
        positionalIndex.buildInverseIndex();
        
        documents = positionalIndex.getDocNames();
    }
	
    public List<String> topKDocs(String query, int k) {
    	
    	List<DocWithScore> scores = new LinkedList<DocWithScore>();
    	List<String> topKDocs = new LinkedList<String>();
    	double score;
    	
    	for (String doc: documents) {
    		score = positionalIndex.Relevance(query, doc);
    		scores.add(new DocWithScore(doc, score));
    	}
    	
        Collections.sort(scores);
        Collections.reverse(scores);
        
        for (int i = 0; i < k; i++) {
        	topKDocs.add(scores.get(i).getDocName());
        }
    	
        return null;
    }
    
    class DocWithScore implements Comparable<DocWithScore> {
        
    	private final String doc;
        private final Double score;
        
        public DocWithScore(String doc, double score) {
            this.doc = doc;
            this.score = score;
        }
        
        public String getDocName() { return doc; }
        public double getScore() { return score; }

        public int compareTo(DocWithScore obj) {
            return score.compareTo(obj.score);
        }
    }
}
