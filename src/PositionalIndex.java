import java.io.*;
import java.util.*;

public class PositionalIndex
{

    private InverseIndex inverseIndex;
    private int numberOfDocuments;
    private String folderName;
	
    PositionalIndex(String folderName) {
    	this.folderName = folderName;
    	inverseIndex = new InverseIndex();
    	numberOfDocuments = 0;
    }
    
    public void buildInverseIndex() {
    	File folder = new File(folderName);
    	FileReader fileReader = null;
    	BufferedReader bufferedReader = null;
        
        PreProcessing preProcessing = new PreProcessing();
        
        try {
            for (File fileEntry : folder.listFiles()) {
                fileReader = new FileReader(fileEntry);
                bufferedReader = new BufferedReader(fileReader);
                
                String line, content = "";
                numberOfDocuments++;
                
                while((line = bufferedReader.readLine()) != null) {
                    content += line;
                }
                
                String[] terms = preProcessing.run(content);
                inverseIndex.addTerms(fileEntry.getName(), terms);
            }
            
            inverseIndex.computeDocLenghts();
                
        } catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + folderName + "'");
        } catch (Exception e) {
            System.out.println("Error reading file '" + folderName + "'");
        } finally {        
            if(bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            
            if(fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public int termFrequency(String term, String Doc) { //TODO: does Doc start from capital letter?
    	if (inverseIndex.isItNewDocument(Doc) || inverseIndex.isItNewTerm(term)) return 0;
        return inverseIndex.getTermFrequency(term, Doc);
    }
    
    public int docFrequency(String term) {
    	if (inverseIndex.isItNewTerm(term)) return 0;
        return inverseIndex.getDocFrequency(term);
    }
    
    public Map<String, int[]> postingsList(String s) { //TODO: can this be map?
    	return null;
    }
    
    public double weight(String t, String d) {
    	//TODO: do I need to still cast values?
    	double tf_function = (double) (Math.log(1 + termFrequency(t, d)) / Math.log(2));
    	
    	//TODO: do I still need to divide and cast?
    	double idf_function = (double) (Math.log((double) numberOfDocuments / docFrequency(t)) / Math.log(10));
        
    	return tf_function * idf_function;
    }
    
    public double TPScore(String query, String doc) {
        return 0.0;
    }
    
    public double VSScore(String query, String doc) {
        return 0.0;
    }
    
    public double Relevance(String query, String doc) {
        return 0.6 * TPScore(query, doc) + 0.4 * VSScore(query, doc);
    }
    
    public Set<String> getDocNames() {
    	return inverseIndex.getDocNames();
    }
}
